import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import { environment } from 'environments/environment';

@Injectable()
export class LoginService {
  url = `//${environment.API_URL}/account`;
  id: any;

  constructor(private http: Http, private router: Router) { }

  login(email, password) {
    if (email && password) {
      return this.http.get(`${this.url}/?email=${email}&password=${password}`)
        .map(res => {
          if (res && res !== undefined && res !== null) {
            sessionStorage.setItem('email', res.json()[0].email);
            sessionStorage.setItem('id', res.json()[0].id);
            this.gotoDash();
            this.getId();
            return res.json();
          }
        }
      );
    }
  }

  gotoDash() {
    if (sessionStorage.getItem('id') !== null && sessionStorage.getItem('id') !== 'undefined') {
      this.router.navigate(['/dashboard/clockins/Auditor-General (Local Government)']);
    }
  }

  isActive(): boolean {
    return sessionStorage.getItem('id') !== null && sessionStorage.getItem('id') !== 'undefined';
  }

  getEmail() {
    return sessionStorage.getItem('email');
  }

  getId() {
    return sessionStorage.getItem('id');
  }

  logout() {
    sessionStorage.clear();
    this.router.navigate(['/login']);
  }
}

import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "slice"
})
export class SeparatorPipe implements PipeTransform {
  transform(value: any, sep: string, index?: number, limit?: number): any {
    if ((index && !limit) || limit > index) {
      value = value.split(sep);
      value = value.slice(index, index + 1);
      value = value.join(" ");
      return value;
    } else if (index && limit && !(limit < index)) {
      value = value.split(sep);
      value = value.slice(index, limit);
      value = value.join(" ");
      return value;
    } else {
      value = value.split(sep);
      value = value.join(" ");
      return value;
    }
  }
}

@Pipe({
  name: "isnan"
})
export class IsNanPipe implements PipeTransform {
  transform(value: any): any {
    if (isNaN(value) || value < 1) {
      value = 0;
      return value;
    }
    return value;
  }
}

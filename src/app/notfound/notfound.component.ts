import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notfound',
  template: `
    <div class="fourohfour">
      <div class="ocean">
      </div>

      <div class="main">
        <h1>
          Something's fishy here.
        </h1>
        <h2>
          The page you were trying to reach doesn't exist.
        </h2>
        <p class="help">
          <a href="http://en.wikipedia.org/wiki/HTTP_404">Why</a> might this be happening?
        </p>
      </div>
    </div>
  `,
  styles: []
})
export class NotfoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

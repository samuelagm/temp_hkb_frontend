import { Component, OnInit } from "@angular/core";

// import { AccountsService } from "../../accounts.service";

@Component({
  selector: 'app-accounts-list',
  template: `
    <!--<div class="ui grid">
        <div class="ui row">
            <div class="ui sixteen wide column">
                <button routerLink="/accounts/create" class="btn btn-danger pull-left">
                    <clr-icon shape="add"></clr-icon>
                    Create Account
                </button>
            </div>
        </div>
        <div class="ui row">
            <div class="ui sixteen wide column">
              <div class="card">
                  <div class="card-header" data-background-color="blue">
                      <div class="title"><h4>Accounts</h4></div>
                      <div class="category">
                            <input #loc class="input text" type="text" placeholder="location" (keyup)="getAccountsbyLoc(loc.value)"/>
                            <input #name class="input text" type="text" placeholder="name" (keyup)="getAccountsbyName(name.value)"/>
                      </div>
                  </div>
                  <div class="card-content table-responsive table-full-width">
                      <div class="col-xs-12 table-responsive">
                          <table class="table" *ngIf="accounts">
                              <thead class="thead-default">
                                  <tr>
                                      <th>Name</th> <th>Location</th> <th>Email</th> <th>Employer</th> <th>Admin</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <tr *ngFor="let account of accounts">
                                      <td><a routerLink="/accounts/{{account.id}}" class="text-danger">{{account?.name}}</a></td>
                                      <td>{{account?.address?.suite}}, {{account?.address?.street}} {{account?.address?.city}}</td>
                                      <td>{{account?.email}}</td>
                                      <td>{{account?.company.name}}</td>
                                      <td>
                                          <i class="material-icons" *ngIf="account?.admin" style="color: green">check_circle</i>
                                          <i class="material-icons" *ngIf="!account?.admin" style="color: red">cancel</i>
                                      </td>
                                  </tr>
                              </tbody>
                          </table>
                          <p class="text-danger" *ngIf="!accounts?.length > 0">No Matching account for this query</p>
                      </div>
                  </div>
              </div>
            </div>
        </div>
    </div>-->
  `,
  styleUrls: ['./accounts.component.css']
})

export class AccountslistComponent implements OnInit {
  accounts: any;

  constructor() {}

//   getAccountsbyLoc(loc: string) {
//       this.service.getAccountbyCity(loc).subscribe(res => this.accounts = res)
//   }

//   getAccountsbyName(name: string) {
//       this.service.getAccountbyName(name).subscribe(res => this.accounts = res)
//   }

  ngOnInit() {
    //   this.service.getAccounts().subscribe(res => {
    //     this.accounts = res;
    // },
    // err => console.log(err))
  }
}

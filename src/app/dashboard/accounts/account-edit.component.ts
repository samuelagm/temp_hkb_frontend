import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap'

// import { AccountsService } from "../../accounts.service";

@Component({
  selector: 'app-accounts-edit',
  template: `
    <div class="container" style="padding: 3%">
        <h2 class="text-info">{{account?.name}} profile edit</h2>
        <form>
            <div class="form-group">
                <label for="exampleInputEmail1">Change Email</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Change Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Change Address</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
            <button routerLink="/accounts" type="submit" class="btn btn-info">Submit</button>
        </form>
    </div>
  `,
})

export class AccounteditComponent implements OnInit {
  account: any;
  constructor(private route: ActivatedRoute) {}
  ngOnInit() {
    //   this.route.paramMap.switchMap((param: ParamMap) =>
    //        this.service.getAccount(+param.get('id'))
    //     ).subscribe(res => {
    //       this.account = res;
    //     },
    //         err => console.log(err)
    //     )
    }
}

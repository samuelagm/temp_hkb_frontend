import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, ParamMap } from "@angular/router"
import 'rxjs/add/operator/switchMap'

// import { AccountsService } from "../../accounts.service";

@Component({
  selector: 'app-accounts-detail',
  template: `
    <div class="container" style="padding: 3%">
        <div class="row">
            <div class="card">
                <div class="card-header" data-background-color="blue">
                    <h4 class="title">{{account?.name}}</h4>
                </div>
                <div class="card-content table-responsive table-full-width">
                    {{account|json}}
                    <button routerLink="/accounts/edit/{{account.id}}" class="btn btn-info pull-left">Edit Account</button>
                </div>
            </div>
        </div>
    </div>
  `,
})

export class AccountsdetailComponent implements OnInit {
  account: any;
  constructor(private route: ActivatedRoute) {}
  ngOnInit() {
    //   this.route.paramMap.switchMap((param: ParamMap) =>
    //        this.service.getAccount(+param.get('id'))
    //     ).subscribe(res => {
    //       this.account = res;
    //     },
    //         err => console.log(err)
    //     );
    }
}

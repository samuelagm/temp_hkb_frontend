import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgxChartsModule } from '@swimlane/ngx-charts';

import { ClarityModule } from 'clarity-angular';
import { NglModule } from 'ng-lightning/ng-lightning';
import { ChartsModule } from 'ng2-charts';

/* ACCOUNTS COMPONENT */
import { AccountsComponent } from './accounts/accounts.component';
import { AccountslistComponent } from './accounts/account-list.component';
import { AccountsdetailComponent } from './accounts/account-detail.component';
import { AccounteditComponent } from './accounts/account-edit.component';
import { AccountcreationComponent } from './accounts/account-create.component';

/* CLOCKINS COPONENT */
import { ClockinsComponent } from './clockins/clockins.component';
import { ClockinslistComponent } from './clockins/clockins-list.component';
import { ClockinStaffDetailComponent } from './clockins/clockins-staff-detail.component';
import { ClockinsstaffComponent } from './clockins/clockins-staff.component';
import { ClockinLgaComponent } from './clockins/clockin-lga.component';
import { ClockinCountComponent } from './clockins/clockin-count.component';
import { ClockinStaffClockoutComponent } from './clockins/clockin-staff-clockout.component';

/* LOCATION COMPONENT */
import { LocationComponent } from './location/location.component';
import { LocationlistComponent } from './location/location-list.component';
import { LocationdetailComponent } from './location/location-detail.component';

/* REPORTS COMPONENT */
import { ReportsComponent } from './reports/reports.component';
import { ReportslistComponent } from './reports/reports-list.component';
import { ReportsdetailComponent } from './reports/reports-detail.component';

/* TADS COMPONENT */
import { TadsComponent } from './tads/tads.component';
import { TadlistComponent } from './tads/tads-list.component';
import { TaddetailComponent } from './tads/tad-detail.component';

/*  BASE MODULE */
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';

/* SERVICES */
import { ReportsService } from './reports/reports.service';
import { LocationService } from './location/location.service';
import { TadService } from './tads/tad.service';
import { ClockinsService } from './clockins/clockins.service';

import { SeparatorPipe, IsNanPipe } from '../separator.pipe';

@NgModule({
  imports: [
    CommonModule,
    ClarityModule.forRoot(),
    NglModule.forRoot(),
    DashboardRoutingModule,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule
    // NgxChartsModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  declarations: [
    DashboardComponent,
    AccountsComponent,
    AccountslistComponent,
    AccountsdetailComponent,
    AccounteditComponent,
    AccountcreationComponent,
    ClockinsComponent,
    ClockinslistComponent,
    ClockinStaffDetailComponent,
    ClockinsstaffComponent,
    ClockinLgaComponent,
    ClockinCountComponent,
    ClockinStaffClockoutComponent,
    LocationComponent,
    LocationlistComponent,
    LocationdetailComponent,
    ReportsComponent,
    ReportslistComponent,
    ReportsdetailComponent,
    TadsComponent,
    TadlistComponent,
    TaddetailComponent,
    SeparatorPipe,
    IsNanPipe
  ],
  exports: [SeparatorPipe, FormsModule, ReactiveFormsModule]
  ,
  providers: [
    ReportsService,
    LocationService,
    TadService,
    ClockinsService,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class DashboardModule { }

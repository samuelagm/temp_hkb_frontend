import { Component } from '@angular/core';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  date = new Date();
  time: any;
  collapsed = true;

  constructor(public service: LoginService) {
    setInterval(() => {
      this.time = new Date().toLocaleTimeString();
    }, 1000);
  }

}

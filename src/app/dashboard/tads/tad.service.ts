import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";

import "rxjs/add/operator/map";
import "rxjs/add/operator/filter";

@Injectable()
export class TadService {
  url = "http://41.184.176.90:8083/api/tad/?organisation=59232bc5f705482100f47e17";
  header = new Headers({ "Content-type": "application/json" });

  constructor(private http: Http) {}

  getTads() {
    return this.http
      .get(
        `${
          this.url
        }&sort=name%20ASC&where={"name": {"!": ""}}&populate=location`
      )
      .map(res => res.json());
  }

  getTad(id: any) {
    return this.http.get(`${this.url}&id=${id}`).map(res => res.json());
  }
}

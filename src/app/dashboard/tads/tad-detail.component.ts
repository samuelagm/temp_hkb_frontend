import { Component, Input, OnInit, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { ISubscription } from 'rxjs/Subscription';
import { TadService } from "./tad.service";

@Component({
  selector: "app-tad-detail",
  template: `
    <clr-modal [(clrModalOpen)]="id" [clrModalClosable]="false" [clrModalStaticBackdrop]="true" [clrModalSize]="'xl'">
        <h3 class="modal-title">{{tad?.name|titlecase}}</h3>
        <div class="modal-body">
            {{tad|json}}
        </div>
        <div class="modal-footer">
          <button class="btn btn-danger" (click)="close()">close</button>
        </div>
    </clr-modal>
    `,
  styleUrls: ["./tads.component.css"]
})

export class TaddetailComponent implements OnDestroy {
  tad: any;
  subscriber: ISubscription;
  @Input() id: any;

  constructor(private service: TadService, private cd: ChangeDetectorRef) {}

  getTad(id) {
    this.id = id;
    this.subscriber = this.service.getTad(this.id).subscribe(res => {
      this.tad = res;
      this.cd.detectChanges();
    });
  }

  close() {
    this.id = '';
    this.cd.detectChanges();
  }

  ngOnDestroy() {
    if (typeof(this.tad) !== "undefined") {
      this.subscriber.unsubscribe();
    }
  }
}

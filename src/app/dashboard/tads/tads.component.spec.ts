import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TadsComponent } from './tads.component';

describe('TadsComponent', () => {
  let component: TadsComponent;
  let fixture: ComponentFixture<TadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TadsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";
import { TaddetailComponent } from "./tad-detail.component";
import { TadService } from "./tad.service";

import { ISubscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/from';
import { Observable } from "rxjs/Observable";

@Component({
  selector: "app-tads-list",
  template: `
  <div class="ui stackable grid">
    <div class="ui row">
        <div class="ui sixteen wide column">
            <div *ngIf="!tads" class="progress loop" style="width: 100%; height: 0.2%"><progress></progress></div>
            <h2>TADS</h2>
            <hr>
            <clr-datagrid id="datagrid">
                <clr-dg-column [clrDgField]="'name'">Name</clr-dg-column>
                <clr-dg-column [clrDgField]="'internet_provider'">Internet Provider</clr-dg-column>
                <clr-dg-column [clrDgField]="'caretaker_name'">Caretaker(s)</clr-dg-column>
                <clr-dg-column [clrDgField]="'caretaker_phone'">Caretaker(s) Phone</clr-dg-column>
                <clr-dg-column [clrDgField]="'tad_code'">Tad Code</clr-dg-column>

                <clr-dg-row *clrDgItems="let datum of tads; let even=even; let odd=odd">
                    <clr-dg-cell>
                        <a (click)="select(datum)">{{datum.name|titlecase}}</a>
                    </clr-dg-cell>
                    <clr-dg-cell [ngClass]="{'odd': odd, 'even': even}">{{datum.internet_provider}}</clr-dg-cell>
                    <clr-dg-cell [ngClass]="{'odd': odd, 'even': even}">{{datum.caretaker_name|titlecase}}</clr-dg-cell>
                    <clr-dg-cell [ngClass]="{'odd': odd, 'even': even}">{{datum.caretaker_phone}}</clr-dg-cell>
                    <clr-dg-cell [ngClass]="{'odd': odd, 'even': even}">{{datum.tad_code}}</clr-dg-cell>
                </clr-dg-row>

                <clr-dg-footer>
                    {{pagination.firstItem + 1}} - {{pagination.lastItem + 1}}
                    of {{pagination.totalItems}} tads
                    <clr-dg-pagination #pagination [clrDgPageSize]="10"></clr-dg-pagination>
                </clr-dg-footer>
            </clr-datagrid>
        </div>
    </div>
    <app-tad-detail></app-tad-detail>
  `,
  styleUrls: ["./tads.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TadlistComponent implements OnInit, AfterViewInit, OnDestroy {
  tads: any;
  selectedTad: any;
  subscriber: ISubscription;

  @ViewChild(TaddetailComponent) detail: TaddetailComponent;

  constructor(private service: TadService, private cd: ChangeDetectorRef) {}

  select(tad) {
    this.selectedTad = true;
    this.detail.getTad(tad.id);
  }

  ngAfterViewInit() {
    this.cd.detach();
    document.getElementById('datagrid').addEventListener('click', (e) => {
      this.cd.detectChanges();
    });
  }

  ngOnInit() {
    if (localStorage.getItem('tads') === null) {
      let statetads = [];
      this.subscriber = this.service.getTads().subscribe(res => {
        this.tads = res;
        this.cd.detectChanges();
        res.forEach(tad => {
          if (tad && tad.location && tad.location.lga) {
            statetads.push(tad);
            this.cd.detectChanges();
          }
        });
        localStorage.setItem('tads', JSON.stringify(statetads));
        this.tads = statetads;
      });
    }
    if (localStorage.getItem('tads') !== null) {
      let statetads = [];
      this.subscriber = Observable.from(JSON.parse(localStorage.getItem('tads')))
      .map(tad => {let tadlist = []; tadlist.push(tad); return tadlist;})
      .subscribe(res => {
        if (res) {
          res.forEach(taditem => {
            statetads.push(taditem);
            this.cd.detectChanges();
          });
          this.tads = statetads;
        }
      });
    }
  }

  ngOnDestroy() {
    this.subscriber.unsubscribe();
  }
}

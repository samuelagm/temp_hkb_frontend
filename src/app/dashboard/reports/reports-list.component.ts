import { Component, OnInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef, AfterViewInit, OnDestroy } from "@angular/core";
import { ReportsService } from "./reports.service";
import { ReportsdetailComponent } from "./reports-detail.component";

import { ISubscription } from 'rxjs/Subscription';
import { Client } from 'elasticsearch';
import { FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: "app-reports-list",
  templateUrl: "./reports-list.component.html",
  styleUrls: ["./reports.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class ReportslistComponent implements OnInit, AfterViewInit, OnDestroy {

  client: Client;
  data: any;
  subscriber: ISubscription;
  @ViewChild(ReportsdetailComponent) detail: ReportsdetailComponent;
  submitted = false;
  searchForm = new FormGroup({
    q: new FormControl('', Validators.required),
    category: new FormControl('', Validators.required),
  });

  constructor(private service: ReportsService, private cd: ChangeDetectorRef) { }



  select(item: any) {
    /*     this.detail.getReport(item.id);
        this.cd.detectChanges(); */
  }

  // getRepbyLoc(loc) {
  //   this.service.getDatumbyLg(loc).subscribe(res => (this.data = res));
  // }

  ngOnDestroy() {
    //this.subscriber.unsubscribe();
  }

  ngAfterViewInit() {
    /*     this.cd.detach();
        document.getElementById('datagrid').addEventListener('click', (e) => {
          this.cd.detectChanges();
        }); */
  }

  ngOnInit() {
    /*     this.subscriber = this.service.getData().subscribe(res => {
          this.data = res;
          this.cd.detectChanges();
        }); */

  }


  onSubmit() {
    console.log("King of the Jungle", this.searchForm.controls["category"].value);
    this.service
      .fullTextSearch(this.searchForm.controls["category"].value, this.searchForm.controls["q"].value)
      .then(searchResponse => {
        this.data = searchResponse.hits.hits.map(x => x._source);
      })
  }
}

// .sort(function(self, next) {
//             let comp1 = self.location_name.toUpperCase();
//             let comp2 = next.location_name.toUpperCase();
//             if (comp1 < comp2) {
//               return -1;
//             }
//             if (comp1 > comp2) {
//               return 1;
//             }
//             return 0;});

import { Component, OnInit, Input, AfterViewInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, ParamMap } from "@angular/router";
import { ISubscription } from 'rxjs/Subscription';

import { ReportsService } from './reports.service';

@Component({
  selector: 'app-reports-detail',
  templateUrl: './reports-detail.component.html',
  styleUrls: ['./reports.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class ReportsdetailComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input() id: any;
  datum: any;
  subscriber: ISubscription;

  constructor(private dataService: ReportsService, private route: ActivatedRoute, private cd: ChangeDetectorRef) { }

  getReport(id) {
    this.id = id;
    this.subscriber = this.dataService.getDatum(this.id).subscribe(res => {
      this.datum = res;
      this.cd.detectChanges();
    });
  }

  close() {
    this.id = '';
    this.cd.detectChanges();
  }

  ngAfterViewInit() {
    this.cd.detach();
  }

  ngOnDestroy() {
    if (typeof(this.datum) !== "undefined") {
      this.subscriber.unsubscribe();
    }
  }

  ngOnInit() {  }

}

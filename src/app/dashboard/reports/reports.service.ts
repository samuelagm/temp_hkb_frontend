import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Client, SearchResponse } from 'elasticsearch';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/retry';

@Injectable()
export class ReportsService {
    url = 'http://41.184.176.90:8083/api/report';

    client: Client;

    constructor(private http: Http) { 
        this.connect();
    }

    private connect() {
        this.client = new Client({
            host: 'http://tanda.heckerbella.com:9200'
        });
    }

    getData() {
        return this.http.get(`${this.url}/?sort=createdAt%20DESC&limit=1000&where={"location_name": {"!": ""}}`).map(res => res.json())
    }

    getDatum(id: number) {
        return this.http.get(`${this.url}/${id}`).map(res => res.json());
    }



    fullTextSearch(_field, _queryText): Promise<SearchResponse<{}>> {
        return this.client.search({
            index: "tar",
            type: "attendance",
            filterPath: ['hits.hits._source', 'hits.total', '_scroll_id'],
            body: {
                'query': {
                    'match_phrase_prefix': {
                        [_field]: _queryText,
                    }
                }
            }
        });
    }
}

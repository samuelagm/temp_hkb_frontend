import { Component, Input, ChangeDetectionStrategy, ChangeDetectorRef, AfterViewInit, OnDestroy } from '@angular/core';
import { ISubscription } from 'rxjs/Subscription';

import { LocationService } from './location.service';

@Component({
    selector: 'app-location-detail',
    template: `
    <clr-modal [(clrModalOpen)]="id" [clrModalClosable]="false" [clrModalStaticBackdrop]="true" [clrModalSize]="'xl'">
      <h3 class="modal-title">{{location?.name|titlecase}}</h3>
      <div class="modal-body">
        {{location|json}}
      </div>
      <div class="modal-footer">
        <button class="btn btn-danger" (click)="close()">close</button>
      </div>
    </clr-modal>`,
    styleUrls: ['./location.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class LocationdetailComponent implements AfterViewInit, OnDestroy {
    location: any;
    subscriber: ISubscription;
    @Input() id: string;

    constructor(private service: LocationService, private cd: ChangeDetectorRef) {}

    close() {
      this.id = '';
      this.cd.detectChanges();
    }

    getLocation(id) {
        this.id = id;
        this.subscriber = this.service.getLocation(this.id).subscribe(res => {
          this.location = res;
          this.cd.detectChanges();
        });
    }

    ngAfterViewInit() {
      this.cd.detach();
    }

    ngOnDestroy() {
      if (typeof(this.location) !== "undefined") {
        this.subscriber.unsubscribe();
      }
    }
}

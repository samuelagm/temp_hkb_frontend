import {
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from "@angular/core";
import { LocationService } from "./location.service";
import { LocationdetailComponent } from "./location-detail.component";

import { ISubscription } from "rxjs/Subscription";

@Component({
  selector: "app-clockins-list",
  template: `
  <div class="ui stackable grid">
    <div class="ui row">
      <div class="ui sixteen wide column">
          <div *ngIf="!locations" class="progress loop" style="width: 100%; height: 0.2%"><progress></progress></div>
          <h2>Locations</h2>
          <hr>
          <clr-datagrid id="datagrid">
              <clr-dg-column [clrDgField]="'name'">Location Name</clr-dg-column>
              <clr-dg-column [clrDgField]="'address'">Address</clr-dg-column>
              <clr-dg-column [clrDgField]="'type'">Type</clr-dg-column>
              <clr-dg-column [clrDgField]="'lga'">Lga</clr-dg-column>

              <clr-dg-row *clrDgItems="let datum of locations; let even=even; let odd=odd">
                  <clr-dg-cell><a (click)="select(datum)">{{datum.name|titlecase}}</a></clr-dg-cell>
                  <clr-dg-cell [ngClass]="{'odd': odd, 'even': even}">{{datum.address|titlecase}}</clr-dg-cell>
                  <clr-dg-cell [ngClass]="{'odd': odd, 'even': even}">{{datum.type|titlecase}}</clr-dg-cell>
                  <clr-dg-cell [ngClass]="{'odd': odd, 'even': even}">{{datum.lga|titlecase}} Local Government</clr-dg-cell>
              </clr-dg-row>

              <clr-dg-footer>
                  {{pagination.firstItem + 1}} - {{pagination.lastItem + 1}}
                  of {{pagination.totalItems}} location
                  <clr-dg-pagination #pagination [clrDgPageSize]="10"></clr-dg-pagination>
              </clr-dg-footer>
          </clr-datagrid>
      </div>
  </div>
</div>
<app-location-detail></app-location-detail>
  `,
  styleUrls: ["./location.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LocationlistComponent implements OnInit, AfterViewInit, OnDestroy {
  subscriber: ISubscription;
  locations: any;
  @ViewChild(LocationdetailComponent) detail: LocationdetailComponent;

  constructor(
    private service: LocationService,
    private cd: ChangeDetectorRef
  ) {}

  select(loc) {
    this.detail.getLocation(loc.id);
    this.cd.detectChanges();
  }

  ngAfterViewInit() {
    this.cd.detach();
    document.getElementById('datagrid').addEventListener('click', () => {
      this.cd.detectChanges();
    });
  }

  ngOnInit() {
    this.subscriber = this.service.getLocations().subscribe(
      res => {
        if (res) {
          this.locations = res;
          this.cd.detectChanges();
        }
      },
      err => console.log(err)
    );
  }

  ngOnDestroy() {
    this.subscriber.unsubscribe();
  }
}

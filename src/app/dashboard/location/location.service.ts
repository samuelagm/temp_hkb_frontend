//import { Header } from 'clarity-angular/src';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import { environment } from 'environments/environment';

@Injectable()
export class LocationService {
    url = `${environment.API_URL}/location`;
    //headers = new Headers({'Content-type': 'application/json'});

    constructor(private http: Http) {}

    getLocations() {
        return this.http
            .get(`${this.url}?sort=name%20ASC&skip=1`)
            .map(res => res.json())
            .filter(data => data.type !== 'Other');
    }

    getLocation(id: any) {
        return this.http
            .get(`${this.url}&id=${id}`)
            .map(res => res.json());
    }
}
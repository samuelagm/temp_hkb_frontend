import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { LoginGuard } from "../login.guard";

import { DashboardComponent } from "./dashboard.component";

/* ACCOUNTS COMPONENT */
import { AccountsComponent } from "./accounts/accounts.component";
import { AccountslistComponent } from "./accounts/account-list.component";

/* CLOCKINS COPONENt */
import { ClockinsComponent } from "./clockins/clockins.component";
import { ClockinslistComponent } from "./clockins/clockins-list.component";
import { ClockinLgaComponent } from "./clockins/clockin-lga.component";
import { ClockinStaffDetailComponent } from "./clockins/clockins-staff-detail.component";

/* LOCATION COMPONENT */
import { LocationComponent } from "./location/location.component";
import { LocationlistComponent } from "./location/location-list.component";

/* REPORTS COMPONENT */
import { ReportsComponent } from "./reports/reports.component";
import { ReportslistComponent } from "./reports/reports-list.component";

/* TADS COMPONENT */
import { TadsComponent } from "./tads/tads.component";
import { TadlistComponent } from "./tads/tads-list.component";

const dashroutes: Routes = [
  {
    path: "",
    component: DashboardComponent,
    // canActivateChild: [LoginGuard],
    children: [
      {
        path: "accounts",
        component: AccountslistComponent
      },
      {
        path: "clockins/:mda",
        component: ClockinLgaComponent
      },
      {
        path: "clockins",
        component: ClockinslistComponent
      },
      {
        path: "locations",
        component: LocationlistComponent
      },
      {
        path: "reports",
        component: ReportslistComponent
      },
      {
        path: "tads",
        component: TadlistComponent
      },
      {
        path: "",
        redirectTo: "/dashboard/clockins"
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(dashroutes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {}

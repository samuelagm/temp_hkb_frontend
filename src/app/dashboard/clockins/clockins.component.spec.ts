import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClockinsComponent } from './clockins.component';

describe('ClockinsComponent', () => {
  let component: ClockinsComponent;
  let fixture: ComponentFixture<ClockinsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClockinsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClockinsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

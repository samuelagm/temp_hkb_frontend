import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { Observable } from "rxjs/Observable";

import "rxjs/add/operator/map";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/scan";
import "rxjs/add/operator/share";
import "rxjs/add/operator/publishReplay";
import "rxjs/add/observable/from";
import { environment } from "environments/environment";

@Injectable()
export class ClockinsService {
  url = `${environment.API_URL}/clockin/`;
  url2 = `${environment.API_URL}/location`;
  headers = new Headers({ "Content-type": "application/json" });

  k = new Date();
  time = new Date(
    this.k.getFullYear(),
    this.k.getMonth(),
    this.k.getDate() + 1,
    this.k.setTime(0)
  )
    .toISOString()
    .split("T")[0];

  today = new Date().toISOString().split("T")[0];
 
  constructor(private http: Http) { }

  getYesterday() {
    let yesterday: any;
    const day = new Date();
    yesterday = new Date(day.setDate(day.getDate() - 1))
      .toISOString()
      .split("T")["0"];
    return yesterday;
  }

  // getToday() {
  //   const day = new Date();
  //   const today = new Date(day.setDate(day.getDate())).toISOString().split('T')['0'];
  //   return today;
  // }

  getCachedTads(): Observable<any> {
    return Observable.from(JSON.parse(localStorage.getItem('tads')));
  }

  getHour(timestamp) {
    return new Date(timestamp).getHours();
  }

  getTimestampTime(timestamp) {
    return new Date(timestamp).toISOString();
  }

  getClockinCnt(loc_id) {
    return this.http
      .get(
      `${
      this.url
      }?where={"location":"${loc_id}", "clockInType": {"startsWith": "morning"}, "createdAt": {">": "${
      this.today
      }"}}`
      )
      .map(res => {
        return res.json();
      })
      .map(result => {
        let unique = [];
        for (let i of result) {
          if (
            unique.indexOf(i.staffId) === -1 &&
            this.getHour(i.timestamp) < 13 &&
            this.getTimestampTime(i.timestamp) > this.today
          ) {
            unique.push(i.staffId);
          }
        }
        return unique;
      })
      .share();
  }

  getClockoutCnt(loc_id) {
    return this.http
      .get(
      `${
      this.url
      }?where={"location":"${loc_id}", "clockInType": {"startsWith": "afternoon"}, "createdAt": {">": "${
      this.today
      }"}}`
      )
      .map(res => res.json())
      .map(result => {
        let unique = [];
        for (let i of result) {
          if (
            unique.indexOf(i.staffId) === -1 &&
            this.getHour(i.timestamp) >= 13 &&
            this.getTimestampTime(i.timestamp) > this.today
          ) {
            unique.push(i.staffId);
          }
        }
        return unique;
      })
      .share();
  }

  getClockinsByLoc(loc_id) {
    return this.http
      .get(
      `${
      this.url
      }?sort=timestamp%20ASC&where={"location":"${loc_id}", "clockInType": {"startsWith": "morning"}, "createdAt": {">": "${
      this.time
      }"}}&populate=location,tad`
      )
      .map(res => Array.from(new Set(res.json().map(item => item))));
  }

  getClockoutsByLoc(loc_id) {
    return this.http
      .get(
      `${
      this.url
      }?sort=timestamp%20ASC&where={"location":"${loc_id}", "clockInType": {"startsWith": "afternoon"}, "createdAt": {">": "${
      this.time
      }"}}&populate=location,tad`
      )
      .map(res => Array.from(new Set(res.json().map(item => item))));
  }

  getLocStaffCount(loc_id) {
    return this.http
      .get(
      `${
      this.url
      }?where={"location":"${loc_id}", "staffFirstName": {"!": ""}}`
      )
      .map(res => Array.from(new Set(res.json().map(item => item.staffId))))
      .share();
  }

  getLocation(mda) {
    return this.http
      .get(
      `${
      this.url2
      }?sort=name%20ASC&where={"MDA": {"startsWith": "${mda}"}, "name": {"!": ""}}`
      )
      .map(res => res.json());
  }

  getStaffClockin(staffId) {
    return this.http
      .get(
      `${
      this.url
      }?where={"staffId": "${staffId}", "clockInType": {"startsWith": "morning"}}`
      )
      .map(res => res.json())
      .share();
  }

  getStaffMonthClockin(staffId, month, year) {
    return this.http
      .get(
      `${
      this.url
      }?where={"staffId": "${staffId}", "clockInType": {"startsWith": "morning"}, "year": "${year}", "month": "${month}"}`
      )
      .map(res => res.json())
      .share();
  }

  getStaffClockout(staffId) {
    return this.http
      .get(
      `${
      this.url
      }?where={"staffId": "${staffId}", "clockInType": {"startsWith": "afternoon"}}`
      )
      .map(res => res.json())
      .share();
  }

  getStaffMonthClockout(staffId, month, year) {
    return this.http
      .get(
      `${
      this.url
      }?where={"staffId": "${staffId}", "clockInType": {"startsWith": "afternoon"}, "year": "${year}", "month": "${month}"}`
      )
      .map(res => res.json())
      .share();
  }

  getStaffLatestClockout(staffId) {
    return this.http
      .get(
      `${
      this.url
      }?limit=1&sort=createdAt%20DESC&where={"staffId": "${staffId}", "clockInType": {"startsWith": "afternoon"}, "createdAt": {">": "${
      this.today
      }"}}`
      )
      .map(res => res.json()[0])
      .filter(resp => this.getTimestampTime(resp.timestamp) > this.today)
      .share();
  }

  getStaffLatestClockin(staffId) {
    return this.http
      .get(
      `${
      this.url
      }?limit=1&sort=createdAt%20DESC&where={"staffId": "${staffId}", "clockInType": {"startsWith": "morning"}, "createdAt": {">": "${
      this.today
      }"}}`
      )
      .map(res => res.json()[0])
      .filter(resp => this.getTimestampTime(resp.timestamp) > this.today)
      .share();
  }
}

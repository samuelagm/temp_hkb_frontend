import { Component } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { Router } from "@angular/router";
import { MDAs } from "./MDA";

@Component({
  selector: "app-clockins-list",
  template: `
  <div class="ui stackable grid">
    <div class="ui row">
      <div class="ui sixteen wide column">
        <h2>Clockins for {{today | date:"fullDate"}}, {{today | date:"mediumTime"}}</h2>
        <clr-datagrid id="datagrid">
          <clr-dg-column [clrDgField]="'name'">L.G.A</clr-dg-column>
          <clr-dg-column [clrDgField]="'deployedOffice'">Staff</clr-dg-column>
          <clr-dg-column [clrDgField]="'staffId'">Clock-ins [%]</clr-dg-column>
          <clr-dg-column [clrDgField]="'clockInType'">Clock-outs [%]</clr-dg-column>
          <clr-dg-column [clrDgField]="'createdAt'">Defaults</clr-dg-column>

          <ng-container *ngIf="lgs?.length">
            <clr-dg-row *clrDgItems="let lg of lgs; let even=even; let odd=odd">
              <clr-dg-cell><a (click)="goToLga(lg)" [routerLink]="['.', lg]">{{lg}}</a></clr-dg-cell>
              <clr-dg-cell [ngClass]="{'odd': odd, 'even': even}">
                  <app-staff-clockin #staff [lga]="lg.toLowerCase()"></app-staff-clockin>
              </clr-dg-cell>
              <clr-dg-cell [ngClass]="{'odd': odd, 'even': even}">
                  <app-clockin-count *ngIf="staff?.count > 500" #clockin [lg]="lg.toLowerCase()"
                  [tadarray]="staff?.ids" [clockin]="true"></app-clockin-count>
                  <span *ngIf="staff?.count < 500" class="spinner spinner-sm"></span>
                  <span *ngIf="staff?.count > 500" >[ {{ clockin?.cin_cnt / staff?.count * 100 | number:'1.1-2' | isnan }}% ]</span>
              </clr-dg-cell>
              <clr-dg-cell [ngClass]="{'odd': odd, 'even': even}">
                  <app-clockin-count *ngIf="staff?.count > 500" #clockout [lg]="lg.toLowerCase()"
                  [tadarray]="staff?.ids" [clockin]="false"></app-clockin-count>
                  <span *ngIf="staff?.count < 500" class="spinner spinner-sm"></span>
                  <span *ngIf="staff?.count > 500" >[ {{ clockout?.cout_cnt / staff?.count * 100 | number:'1.1-2' | isnan }}% ]</span>
              </clr-dg-cell>
              <clr-dg-cell [ngClass]="{'odd': odd, 'even': even}">
                <span *ngIf="staff?.count > 500" >
                  {{ getMax(staff?.count,clockout?.cout_cnt,clockin?.cin_cnt) / staff?.count * 100 | number:'1.1-2' | isnan }}%
                </span>
                <span *ngIf="staff?.count < 500" class="spinner spinner-sm"></span>
              </clr-dg-cell>
            </clr-dg-row>
          </ng-container>

          <clr-dg-footer>
            {{pagination.firstItem + 1}} - {{pagination.lastItem + 1}} of {{pagination.totalItems}} clockins
            <clr-dg-pagination #pagination [clrDgPageSize]="10"></clr-dg-pagination>
          </clr-dg-footer>
        </clr-datagrid>
      </div>
    </div>
  </div>
  `,
  styleUrls: ["./clockins.component.css"],
})

export class ClockinslistComponent {
  lgs = MDAs;
  today = new Date();

  constructor(private titleService: Title, private router: Router) {
    this.titleService.setTitle("Disc Tanda | Clock-ins");
  }

  goToLga(lg) {
    this.router.navigate(["dashboard/clockins", lg]);
  }

  getMax(minued, a, b) {
    const val = minued - Math.max(a, b);
    return val;
  }
}

import {
  Component,
  Input,
  OnInit,
  OnDestroy,
  AfterViewInit,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  SimpleChanges
} from "@angular/core";
import { ClockinsService } from "./clockins.service";
import { ISubscription } from "rxjs/Subscription";
@Component({
  selector: "app-staff-clockout",
  template: `{{clockout}}`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClockinStaffClockoutComponent implements AfterViewInit, OnDestroy {
  clockout: any;
  @Input() staffId;
  subscriber: ISubscription;

  constructor(private service: ClockinsService, private cd: ChangeDetectorRef) {
    this.clockout = "--:--:--";
  }

  setTime(timestamp) {
    return new Date(timestamp).toLocaleTimeString();
  }

  ngAfterViewInit() {
    this.cd.detach();
    this.subscriber = this.service
      .getStaffLatestClockout(this.staffId)
      .subscribe(res => {
        if (res && res.timestamp) {
          this.clockout = this.setTime(res.timestamp);
          this.cd.detectChanges();
        }
      }, error => (this.clockout = "--:--:--"));
  }

  ngOnDestroy() {
    this.subscriber.unsubscribe();
  }
}

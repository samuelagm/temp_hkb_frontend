import { ISubscription } from "rxjs/Subscription";
import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  OnDestroy
} from "@angular/core";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { Title } from "@angular/platform-browser";
import { ClockinsService } from "./clockins.service";
import { ClockinStaffDetailComponent } from "./clockins-staff-detail.component";
import { Observable } from "rxjs/Observable";
import { MDAs } from "./MDA";
import { ClockinsstaffComponent } from "app/dashboard/clockins/clockins-staff.component";
import { ClockinCountComponent } from "app/dashboard/clockins/clockin-count.component";
import * as _ from 'lodash';

@Component({
  selector: "app-clockins-list",
  templateUrl: "./clockin-lga.component.html",
  styleUrls: ["./clockins.component.css"],
})
export class ClockinLgaComponent implements OnInit, AfterViewInit, OnDestroy {
  clockins: any;
  @ViewChild(ClockinStaffDetailComponent) detail: ClockinStaffDetailComponent;
  @ViewChild(ClockinCountComponent) countcomp: ClockinCountComponent;
  @ViewChild(ClockinsstaffComponent) staffcomp: ClockinsstaffComponent;
  locations: any;
  count: number;
  locale: any;
  data: boolean;
  close: any;
  shownav: boolean;
  shaype: string;
  tiny: boolean;
  message: string;
  today = new Date();
  lg: any;
  lgs = MDAs;
  subscriber: ISubscription;
  secsubscriber: ISubscription;

  constructor(
    private service: ClockinsService,
    private route: ActivatedRoute,
    private title: Title
  ) {
    this.tiny = false;
    this.message = "";
  }

  checkScreenWidth() {
    if (window.innerWidth < 800) {
      this.tiny = true;
    } else {
      this.tiny = false;
    }
  }

  select(clockin) {
    this.detail.getClockin(clockin);
  }

  toggleNav() {
    this.shownav = !this.shownav;
    this.shaype = this.shownav ? "bars" : "times";
  }

  goToLga(lg) {
    this.clockins = "";
    this.data = false;
    this.locations = [];
    const div = document.querySelector(".content-area") as HTMLDivElement;
    div.style.opacity = "0.3";
    setTimeout(() => {
      div.style.opacity = "1";
      div.scroll(0, 0);
      this.staffcomp.getStaffCount();
      this.countcomp.getClockinCount();
    }, 500);
  }

  getMax(minued, a, b) {
    let val: number;
    val = minued - Math.max(a, b);
    return val;
  }

  getPerc(inst, tok) {
    const perc = inst / tok * 100;
    return perc;
  }

  getTime(dtstr) {
    return new Date(dtstr).toLocaleTimeString();
  }

  getLocClockins(loc) {
    this.data = true;
    this.message = "Retrieving data for selected location";
    this.subscriber = this.service.getClockinsByLoc(loc).subscribe(
      res => {
        if (res.length > 0) {
          this.data = false;
          this.clockins = res;
          this.message = "";
          this.count = res.length;
          this.locale = res[0];
         //console.log(_.groupBy(res, "FullName")) 
        } else {
          this.message =
            "No clock-ins have been recorded in this location today";
          this.data = false;
          this.clockins = 0;
          this.count = null;
          this.locale = null;
        }
      },
      err => {
        this.message =
          "Unexpected Error! We are unable to retrieve data fro this location at this time";
        this.data = false;
        this.clockins = 0;
        this.count = null;
        this.locale = null;
        alert(`Couldn't retrieve data`);
      }
    );
  }

  tracker(index: number, item: any) {
    return item.staffId;
  }

  ngAfterViewInit() {

  }

  ngOnDestroy() {
    if (this.clockins && this.clockins !== undefined) {
      this.subscriber.unsubscribe();
    }
    this.secsubscriber.unsubscribe();
  }

  ngOnInit() {
    this.data = false;
    this.shownav = false;
    this.shaype = "times";
    this.route.paramMap.subscribe(param => {
      this.clockins = "";
      this.data = false;
      this.locations = [];
      this.lg = param.get("mda");
      this.secsubscriber = this.service
        .getLocation(param.get("mda"))
        .subscribe(res => {
          if (res) {
            this.data = true;
            this.locations = res;
            console.log(res)
          }
        });
      this.title.setTitle(`${param.get("mda")} | Time & Attendance Data`);
    });
  }
}

import {
  Component,
  AfterViewInit,
  Input,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  OnDestroy,
  OnInit
} from "@angular/core";
import { ISubscription } from "rxjs/Subscription";
import { ClockinsService } from "./clockins.service";

import "rxjs/add/operator/map";
import "rxjs/add/operator/filter";

@Component({
  selector: "app-staff-clockin",
  template: `{{count}}`,
  styleUrls: ["./clockins.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClockinsstaffComponent
  implements AfterViewInit, OnDestroy, OnInit {
  @Input() lga: string;
  count = 0;
  subscriber: ISubscription;
  subsubscriber: ISubscription[] = [];
  ids = [];

  constructor(
    private cd: ChangeDetectorRef,
    private service: ClockinsService
  ) {}

  getStaffCount() {
    this.count = 0;
    this.cd.detectChanges();
    if (localStorage.getItem("tads") === null) {
      this.subscriber = this.service.getLocation(this.lga).subscribe(res => {
        if (res) {
          res.forEach(i => {
            this.ids.push(i.id);
            this.subsubscriber.push(
              this.service.getLocStaffCount(i.id).subscribe(result => {
                if (result && result.length > 0) {
                  this.count += result.length;
                  this.cd.detectChanges();
                }
              })
            );
          });
        }
      });
    }
    if (localStorage.getItem("tads") !== null) {
      this.count = 0;
      this.cd.detectChanges();
      this.subscriber = this.service
        .getCachedTads()
        .filter(
          reslt =>
            reslt.location.name &&
            reslt.location.lga &&
            reslt.location.lga.toLowerCase().includes(this.lga.toLowerCase())
        )
        .map(res => {
          let tads = [];
          tads.push(res);
          return tads;
        })
        .subscribe(resp => {
          if (resp) {
            resp.forEach(tad => {
              this.subsubscriber.push(
                this.service
                  .getLocStaffCount(tad.location.id)
                  .subscribe(staffs => {
                    if (staffs) {
                      this.count += staffs.length;
                      this.cd.detectChanges();
                    }
                  })
              );
            });
          }
        });
    }
  }

  ngOnInit() {}

  ngAfterViewInit() {
    this.cd.detach();
    this.getStaffCount();
  }

  ngOnDestroy() {
    this.subscriber.unsubscribe();
    this.subsubscriber.forEach(subscription => subscription.unsubscribe());
  }
}

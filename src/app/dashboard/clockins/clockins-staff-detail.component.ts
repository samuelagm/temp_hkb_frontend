import { Component, Input, OnInit, OnDestroy } from "@angular/core";
import { ClockinsService } from "./clockins.service";

import * as moment from "moment";
import { ActivatedRoute, ParamMap } from "@angular/router";
import "rxjs/add/operator/switchMap";
import { ISubscription } from "rxjs/Subscription";

@Component({
  selector: "app-clockin-detail",
  templateUrl: "./clockins-staff-detail.component.html",
  styleUrls: ["./clockins.component.css"]
})
export class ClockinStaffDetailComponent implements OnDestroy, OnInit {
  clockin: any;
  clockOut: any;
  staffId: any;
  timelinein = [];
  timelineout = [];
  averagein: any;
  averageout: any;
  labelsin = [];
  labelsout = [];
  open = true;
  month: any;
  noresultin: boolean;
  noresultout: boolean;
  messagein: string;
  messageout: string;
  year = new Date().getFullYear();
  jahr: string;
  disable: boolean;
  loading: boolean;
  subscription: ISubscription;

  months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  lineChartColors = [
    {
      backgroundColor: "rgba(148,159,177,0.2)",
      borderColor: "rgba(148,159,177,1)",
      pointBackgroundColor: "rgba(148,159,177,1)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(148,159,177,0.8)"
    }
  ];

  lineChartColorsout = [
    {
      backgroundColor: "rgba(77,83,96,0.2)",
      borderColor: "rgba(77,83,96,1)",
      pointBackgroundColor: "rgba(77,83,96,1)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(77,83,96,1)"
    }
  ];

  lineChartOptions = {
    responsive: true
  };

  constructor(private service: ClockinsService, private route: ActivatedRoute) {
    this.disable = true;
  }

  enableSelect() {
    if (this.jahr.length > 3) {
      this.disable = false;
    } else {
      this.disable = true;
    }
  }

  sessionDump(payload, title: string) {
    payload = payload !== null ? JSON.stringify(payload) : "";
    if (payload) {
      sessionStorage.setItem(payload, title);
    } else {
      return;
    }
  }

  clear() {
    setTimeout(() => {
      this.staffId = false;
    }, 200);
    this.timelinein = [];
    this.timelineout = [];
    this.labelsin = [];
    this.labelsout = [];
    this.averagein = null;
    this.averageout = null;
    this.clockin = null;
  }

  setTimeFromHour(hour) {
    const k = new Date();
    const time = new Date(
      k.getFullYear(),
      k.getMonth(),
      k.getDate(),
      k.setTime(hour)
    );
    return time;
  }

  isNotWeekend(date) {
    const tVal = new Date(date).getDay() !== 0 && new Date(date).getDay() !== 6;
    return tVal;
  }

  getMonth(i: number) {
    return this.months[i];
  }

  saveCanvas(anchor, canvas) {
    const link = document.getElementById(anchor) as HTMLAnchorElement;
    const source = document.getElementById(canvas) as HTMLCanvasElement;
    link.href = source.toDataURL();
    setTimeout(() => {
      link.click();
    }, 200);
  }

  isPresentMonth(date) {
    const jsDate = new Date();
    const thisMonth = moment().set({
      year: jsDate.getFullYear(),
      month: jsDate.getMonth(),
      date: 1
    });
    return moment(date).isAfter(thisMonth);
  }

  getStaffMonthClockouts(month, year) {
    this.timelineout = [];
    this.labelsout = [];
    this.averageout = null;

    this.service.getStaffMonthClockout(this.staffId, month, year).subscribe(
      res => {
        if (res && res !== undefined && res.length > 1) {
          res.forEach(item => {
            if (
              item !== undefined &&
              item.timestamp &&
              this.isNotWeekend(item.timestamp)
            ) {
              this.timelineout.push(new Date(item.timestamp).getHours());
              this.labelsout.push(
                new Date(item.timestamp).toLocaleDateString()
              );
              const avg = Math.round(
                this.timelineout.reduce((prev, next) => prev + next) /
                  this.timelineout.length
              );
              this.averageout = this.setTimeFromHour(avg);
            }
          });
          this.loading = false;
        }
        if (res.length < 1) {
          this.noresultout = true;
          this.messageout = `No clockout data from ${this.getMonth(
            month
          )} ${year} for this staffer`;
        }
      },
      err => {
        this.noresultin = true;
        this.messageout = `No clockin from ${this.getMonth(
          month
        )} ${year} for this staffer`;
      }
    );
  }

  getStaffMonthClockins(month, year) {
    this.timelinein = [];
    this.labelsin = [];
    this.averagein = null;
    this.month = this.getMonth(month);
    this.year = year;
    this.loading = true;

    this.service.getStaffMonthClockin(this.staffId, month, year).subscribe(
      res => {
        if (res && res !== undefined && res.length > 1) {
          res.forEach(item => {
            if (
              item !== undefined &&
              item.timestamp &&
              this.isNotWeekend(item.timestamp)
            ) {
              this.timelinein.push(new Date(item.timestamp).getHours());
              this.labelsin.push(new Date(item.timestamp).toLocaleDateString());
              const avg = Math.round(
                this.timelinein.reduce((prev, next) => prev + next) /
                  this.timelinein.length
              );
              this.averagein = this.setTimeFromHour(avg);
            }
          });
          this.loading = false;
        }
        if (res.length < 1) {
          this.noresultin = true;
          this.messagein = `No clockin from ${this.getMonth(
            month
          )} ${year} for this staffer`;
          this.loading = false;
        }
      },
      err => {
        this.noresultin = true;
        this.messagein = `No clockin from ${this.getMonth(
          month
        )} ${year} for this staffer`;

        this.loading = false;
      }
    );
  }

  getClockin(id) {
    this.staffId = id;
    this.month = this.months[new Date().getMonth()];
    this.year = new Date().getFullYear();

    this.service.getStaffClockin(this.staffId).subscribe(
      res => {
        if (res && res.length) {
          this.clockin = res[0];
          res.forEach(item => {
            if (
              item !== undefined &&
              item.timestamp &&
              this.isNotWeekend(item.timestamp) &&
              this.isPresentMonth(item.timestamp)
            ) {
              this.timelinein.push(new Date(item.timestamp).getHours());
              this.labelsin.push(new Date(item.timestamp).toLocaleDateString());
              const avg = Math.round(
                this.timelinein.reduce((prev, next) => prev + next) /
                  this.timelinein.length
              );
              this.averagein = this.setTimeFromHour(avg);
            }
          });
        }
        if (!res.length) {
          this.messagein = `No clockin from ${this.month} ${
            this.year
          } for this staffer`;
          this.loading = false;
        }
      },
      err => {
        this.messagein = `No clockin from ${this.month} ${
          this.year
        } for this staffer`;
      }
    );

    this.service.getStaffClockout(this.staffId).subscribe(
      res => {
        if (res && res !== undefined) {
          res.forEach(item => {
            if (
              item !== undefined &&
              item.timestamp &&
              this.isNotWeekend(item.timestamp) &&
              this.isPresentMonth(item.timestamp)
            ) {
              this.timelineout.push(new Date(item.timestamp).getHours());
              this.labelsout.push(
                new Date(item.timestamp).toLocaleDateString()
              );
              const avg = Math.round(
                this.timelineout.reduce((prev, next) => prev + next) /
                  this.timelineout.length
              );
              this.averageout = this.setTimeFromHour(avg);
            }
          });
        }
        if (!res.length) {
          this.messageout = `No clockin from ${this.month} ${
            this.year
          } for this staffer`;
        }
      },
      err => {
        this.messageout = `No clockin from ${this.month} ${
          this.year
        } for this staffer`;
      }
    );
  }

  ngOnDestroy() {}

  ngOnInit() {
    // this.route.paramMap.subscribe(param => {
    //   this.getClockin(param.get("id"));
    // });
  }
}

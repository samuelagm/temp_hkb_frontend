import {
  Component,
  OnDestroy,
  AfterViewInit,
  SimpleChanges,
  Input,
  Output,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from "@angular/core";
import { ISubscription } from "rxjs/Subscription";
import { ClockinsService } from "./clockins.service";

import "rxjs/add/operator/map";
import "rxjs/add/operator/filter";

@Component({
  selector: "app-clockin-count",
  template: `
    <span *ngIf="clockin">{{cin_cnt}}</span>
    <span *ngIf="!clockin">{{cout_cnt}}</span>`,
  styleUrls: ["./clockins.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClockinCountComponent implements OnDestroy, AfterViewInit, OnInit {
  @Input() tadarray: any[];
  @Input() clockin: boolean;
  @Input() lg: string;
  @Input() count: number;
  subscriber: ISubscription;
  subsubscription: ISubscription[] = [];
  cin_cnt = 0;
  cout_cnt = 0;
  perc_in: number;
  perc_out: number;

  constructor(private cd: ChangeDetectorRef, private service: ClockinsService) {
    this.cin_cnt = 0;
    this.cout_cnt = 0;
  }

  getPerc(cnt, inst) {
    return (inst / cnt) * 100;
  }

  getClockinCount() {
    this.cin_cnt = 0;
    this.cout_cnt = 0;
    this.perc_in = 0;
    this.perc_out = 0;
    this.cd.detectChanges();
    if (localStorage.getItem("tads") === null) {
      setTimeout(() => {
        if (this.clockin) {
          this.tadarray.forEach(i => {
            this.subsubscription.push(
              this.service.getClockinCnt(i).subscribe(result => {
                if (result) {
                  this.cin_cnt += result.length;
                  this.perc_in = this.getPerc(this.count, this.cin_cnt);
                  this.cd.detectChanges();
                }
              })
            );
          });
        }
        if (!this.clockin) {
          this.tadarray.forEach(i => {
            this.subsubscription.push(
              this.service.getClockoutCnt(i).subscribe(result => {
                if (result) {
                  this.cout_cnt += result.length;
                  this.perc_out = this.getPerc(+this.count, +this.cout_cnt);
                  this.cd.detectChanges();
                }
              })
            );
          });
        }
      }, 500);
    }
    if (localStorage.getItem("tads") !== null) {
      this.subscriber = this.service
        .getCachedTads()
        .filter(
        res =>
          res.location.name &&
          res.location.lga &&
          res.location.lga.toLowerCase().startsWith(this.lg.toLowerCase())
        )
        .map(resp => {
          let tads = [];
          tads.push(resp);
          return tads;
        })
        .subscribe(reslt => {
          if (reslt) {
            if (this.clockin) {
              reslt.forEach(tad => {
                this.subsubscription.push(
                  this.service
                    .getClockinCnt(tad.location.id)
                    .subscribe(items => {
                      this.cin_cnt += items.length;
                      this.perc_in = this.getPerc(this.count, this.cin_cnt);
                      this.cd.detectChanges();
                      this.cd.detectChanges();
                    })
                );
              });
            }
            if (!this.clockin) {
              reslt.forEach(tad => {
                this.subsubscription.push(
                  this.service
                    .getClockoutCnt(tad.location.id)
                    .subscribe(items => {
                      this.cout_cnt += items.length;
                      this.perc_out = this.getPerc(+this.count, +this.cout_cnt);
                      this.cd.detectChanges();
                    })
                );
              });
            }
          }
        });
    }
  }

  ngAfterViewInit() {
    this.cd.detach();
    this.getClockinCount();
  }

  ngOnInit() { }

  ngOnDestroy() {
    this.subsubscription.forEach(subscription => subscription.unsubscribe());
    if (localStorage.getItem("tads") !== null) {
      this.subscriber.unsubscribe();
    }
  }
}

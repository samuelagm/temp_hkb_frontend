import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { LoginService } from '../login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  message: any;
  email: any;
  password: any;

  constructor(public service: LoginService, private router: Router) { }

  login(email, password) {
    this.service.login(email, password)
      .subscribe(res => {
        if (res && res.id) {
          console.log('Logged In');
          this.message = '';
        }
        if (!res || !res.id) {
          this.message = 'Invalid username or password';
          setTimeout(() => this.message = '', 5000);
        }
      },
      err => {
        this.message = 'Invalid username or password';
        setTimeout(() => this.message = '', 5000);
      }
    );
  }

  ngOnInit() {  }

  ngOnDestroy() {  }
}

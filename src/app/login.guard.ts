import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { LoginService } from './login.service';

@Injectable()
export class LoginGuard implements CanActivate {
  constructor(private service: LoginService, private router: Router, private loc: Location) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.service.isActive()) {
      return Observable.of(true);
    } else {
      setTimeout(() => {
        this.router.navigate(['/login']);
      }, 500);
      console.dir(next);
      console.dir(state);
      return  Observable.of(false);
    }
  }

  canActivateChild (
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
      if (this.service.isActive()) {
        return Observable.of(true);
      } else {
        setTimeout(() => {
          this.router.navigate(['/login']);
        }, 500);
        return  Observable.of(false);
      }
    }

}

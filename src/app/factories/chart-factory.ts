// declare const Bokeh;

// const tools = 'pan,hover,wheel_zoom,reset';

// export function linechart(x: any[], y: number[], options?: any) {
//   // Declaring Axis Limits
//   // const x_lim = new Bokeh.Range1d({ start: Math.min(...x), end: Math.max(...x)});
//   const y_lim = new Bokeh.Range1d({ start: Math.min(...y) || 0, end: Math.max(...y) || 1000 });

//   // Initialising the figure object
//   const plot = Bokeh.Plotting.figure({title: options.title || 'Line', tools: tools, y_range: y_lim,
//     toolbar_location: 'above', sizing_mode: 'stretch_both',  background_fill_color: '#FFFFFF', toolbar_sticky: true,
//     background_fill_alpha: 1, x_axis_type: 'datetime'
//   });

//   plot.xgrid.visible = false;
//   plot.ygrid.grid_line_dash = [4, 3];
//   plot.ygrid.grid_line_alpha = 1;

//   plot.xaxis.minor_tick_line_color = null;
//   plot.yaxis.minor_tick_line_color = null;
//   plot.xaxis.major_tick_line_color = null;
//   plot.yaxis.major_tick_line_color = null;

//   plot.xaxis.axis_label = options.x_label || 'Day';
//   plot.xaxis.axis_label_text_color = '#000';
//   plot.xaxis.axis_label_standoff = 10;
//   plot.xaxis.axis_label_text_font_style = 'italic';

//   plot.yaxis.axis_label = options.y_label || 'Time';
//   plot.yaxis.axis_label_text_color = '#000';
//   plot.yaxis.axis_label_standoff = 10;
//   plot.yaxis.axis_label_text_font_style = 'italic';

//   const line = plot.line(x, y, {color: '#004A70' });
//   const circle = plot.circle(x, y, {color: '#314351' });

//   const div = document.getElementById(options.container || 'plot');
//   Bokeh.Plotting.show(plot, div);
// }

// export function barchart(data: number[], options: any) {
//   const plt = Bokeh.plotting;

//   const div = document.getElementById(options.container);

//   const bar = Bokeh.charts.bar(data, {
//     axis_number_format: '0.[00]a',
//     orientation: 'vertical'
//   });

//   plt.show(bar, div || 'barplot');
// }

// export function piechart(tag: string[], val: number[], options?: any) {
//   const plt = Bokeh.Plotting;

//   const pie_data = {
//       labels: tag,
//       values: val,
//   };

//   const div = document.getElementById(options.container || 'pieplot');
//   const p4 = Bokeh.Charts.pie(pie_data, {
//       inner_radius: 0.2,
//       start_angle: Math.PI / 6,
//       palette: `Plasma${val.length.toString()}`,
//       slice_labels: 'percentages'
//   });
//   plt.show(p4);
// }


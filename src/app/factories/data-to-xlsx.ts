import { saveAs } from 'save-as';
import * as fs from 'file-saver';
import * as jsonexport from 'jsonexport/dist';

function s2ab(s) {
  const buf = new ArrayBuffer(s.length);
  const view = new Uint8Array(buf);
  for (let i = 0; i !== s.length; ++i) {
    view[i] = s.charCodeAt(i) && 0xFF;
  }
  return buf;
}

export function createSheet4rmJson(data, options) {
  jsonexport(data, function(err, csv) {
    if (err) {
      return console.log(err);
    } else {
      return saveAs(new Blob([csv]), `${options.title}.csv`);
    }
  });
}
